<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ProductWithNoImage\Console\Command;

use Kowal\Integracja\Model\NiepowiazaneManagement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Count extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\ProductWithNoImage\lib\MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);

        $total = $this->magentoService->countProducts();
        $output->writeln("Produsts with no image: " . $total);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_productwithnoimage:count");
        $this->setDescription("Display How Many is with no image");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}


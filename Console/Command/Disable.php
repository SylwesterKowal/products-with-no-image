<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ProductWithNoImage\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Disable extends Command
{

    const ENTITY_ID = "entity_id";
    const TEST = "option";

    public function __construct(
        \Kowal\ProductWithNoImage\lib\MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $entity_id = $input->getArgument(self::ENTITY_ID);
        $option = $input->getOption(self::TEST);
        if (!empty($entity_id)) {
            $output->writeln("TEST for entity_id: " . $entity_id);
            $this->magentoService->disableProductsTest($entity_id);
        } else {
            $this->magentoService->disableProducts();
            $output->writeln("Koniec");
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_productwithnoimage:disable");
        $this->setDescription("Disable Products with no image");
        $this->setDefinition([
            new InputArgument(self::ENTITY_ID, InputArgument::OPTIONAL, "Product Entity ID"),
            new InputOption(self::TEST, "-t", InputOption::VALUE_NONE, "Test")
        ]);
        parent::configure();
    }
}


<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\ProductWithNoImage\lib;


use Magento\Framework\App\ResourceConnection;

class MagentoService
{
    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    public function countProducts($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "select count(m.entity_id) from " . $this->_getTableName('catalog_product_entity_int') . " m 
left join " . $this->_getTableName('eav_attribute') . " a on a.entity_type_id = 4 and a.attribute_id = m.attribute_id
where
  a.attribute_code = 'status'
  and m.entity_id in
    (
      select m.entity_id
      from " . $this->_getTableName('catalog_product_entity') . " m
      left join " . $this->_getTableName('catalog_product_entity_media_gallery_value_to_entity') . " a
        on a.entity_id = m.entity_id
      where a.value_id is null
    );

SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql
        );
    }


    /**
     * @param $sku
     * @param $newQty
     */
    public function disableProducts()
    {
        $connection = $this->_getConnection('core_write');

        $sql = "update " . $this->_getTableName('catalog_product_entity_int') . " m 
                left join " . $this->_getTableName('eav_attribute') . " a on a.entity_type_id = 4 and a.attribute_id = m.attribute_id
                set value = 2
                where
                  a.attribute_code = 'status'
                  and m.entity_id in
                    (
                      select m.entity_id
                      from " . $this->_getTableName('catalog_product_entity') . " m
                      left join " . $this->_getTableName('catalog_product_entity_media_gallery_value_to_entity') . " a
                        on a.entity_id = m.entity_id
                      where a.value_id is null
                    );";


        $connection->query($sql);
    }

    public function disableProductsTest($entity_id)
    {
        $connection = $this->_getConnection('core_write');

        $sql = "update " . $this->_getTableName('catalog_product_entity_int') . " m 
                left join " . $this->_getTableName('eav_attribute') . " a on a.entity_type_id = 4 and a.attribute_id = m.attribute_id
                set value = 2
                where
                  a.attribute_code = 'status'
                  and m.entity_id  = ?";


        $connection->query($sql, [$entity_id]);
    }

}
